<?php

namespace App\Console\Commands;

use App\Models\lead;
use Illuminate\Console\Command;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = 'Anton';
        $phone = '123';
        $message = 'hello';

        $lead = new lead();
        $lead->name=$name;
        $lead->phone=$phone;
        $lead->message=$message;

        $lead->save();
    }
}
