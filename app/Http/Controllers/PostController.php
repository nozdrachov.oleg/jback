<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\lead;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PostController extends Controller
{
    public function index(){

        $posts = Post::where('is_published', 1)->get();
        foreach ($posts as $post){
            dump($post->title);
            dd('end');
        }

        $category = Category::find(1);
        $post = Post::find(2);
        $post = new Post();
        $post->title='dasd';
        $post->save();
        $tag = Tag::find(2);
        $post->category_id=1;
        $post->save();
        dd($category->posts);


        return view('post.index', compact('posts'));

    }

    public function saveLead(){
        $name = 'Anton';
        $phone = '123';
        $massage = 'hello';

        $lead = new lead();
        $lead->save();
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request){
        $data = $request->validate([
            'title'=>'string',
            'content'=>'string',
            'image'=>'string',
        ]);
        Post::create($data);
        return redirect()->route('post.index');
    }

    public function show(Post $post){

        return view('post.show', compact('post'));
    }
    public function edit(Post $post){
        return view('post.edit', compact('post'));
    }

    public function update(Post $post, Request $request){
        $data = $request->validate([
            'title'=>'string',
            'content'=>'string',
            'image'=>'string',
        ]);

        $post->update($data);
        return redirect()->route('post.show', $post->id);

    }

    public function destroy(Post $post){
        $post->delete();
        return redirect()->route('post.index');
    }

    public function firstOrCreate(){
        $post = Post::firstOrCreate([
            'title' => 'some title of post from phpstorm'
        ],[
            'title' => 'some title of post from phpstorm',
            'content' => 'some intresting content',
            'image' => 'some imageblabla.jpg',
            'likes' => 1500,
            'is_published' => 1,
        ]);
        dd($post);
    }

    public function updateOrCreate(){
        $post = Post::updateOrCreate([
            'title' => 'New some not title of post from phpstorm'
        ],[
            'title' => 'New some not title of post from phpstorm',
            'content' => 'Newsome not intresting content',
            'image' => 'New some not imageblabla.jpg',
            'likes' => 1500,
            'is_published' => 1,
        ]);
        dd($post);
    }
}
