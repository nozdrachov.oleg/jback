<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $guarded = false;

    public function usersPhone(){
        return $this->hasMany(UsersPhone::class, 'user_id', 'id');
    }
}
