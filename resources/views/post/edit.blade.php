@extends('layuots/maint')
@section('content')
    <div>
        <form action="{{route('post.update', $post->id)}}" method="post">
            @csrf
            @method('patch')
            <div class="mb-3">
                <label for="title" class="form-label">title</label>
                <input type="text" class="form-control" id="title" name="title">
            </div><br>
            <div class="mb-3">
                <label for="content" class="form-label">content</label>
                <textarea type="text" class="form-control" id="content" name="content"></textarea>>
            </div><br>
            <div class="mb-3">
                <label for="image" class="form-label">image</label>
                <input type="text" class="form-control" id="image" name="image">
            </div><br>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
    <div><a href="{{route('post.index')}}">Back</a></div>
@endsection
