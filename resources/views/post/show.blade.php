@extends('layuots/maint')
@section('content')
    <div>
        <div>{{$post->id}}. {{$post->title}}</div>
        <div>{{$post->contetnt}}</div>
        <div><a href="{{route('post.index')}}">Back</a></div>
    </div>
    <div><a href="{{route('post.edit', $post->id)}}">Edit</a></div>
    <div>
        <form action="{{route('post.delete', $post->id)}}" method="post">
            @csrf
            @method('delete')
            <input type="submit" value="DELETE">
        </form>
    </div>
@endsection
