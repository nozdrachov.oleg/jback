<?php


use App\Http\Controllers\aboutController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\mainController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('posts',[PostController::class, 'index'])->name('post.index');
Route::get('posts/create',[PostController::class, 'create'])->name('post.create');
Route::post('posts',[PostController::class, 'store'])->name('post.store');
Route::get('posts/{post}',[PostController::class, 'show'])->name('post.show');
Route::get('posts/{post}/edit',[PostController::class, 'edit'])->name('post.edit');
Route::patch('posts/{post}',[PostController::class, 'update'])->name('post.update');
Route::delete('posts/{post}',[PostController::class, 'destroy'])->name('post.delete');






Route::get('posts/update',[PostController::class, 'update']);
Route::get('posts/delete',[PostController::class, 'delete']);
Route::get('posts/firstOrCreate',[PostController::class, 'firstOrCreate']);
Route::get('posts/updateOrCreate',[PostController::class, 'updateOrCreate']);

Route::get('about',[aboutController::class, 'index'])->name('about.index');
Route::get('contact',[contactController::class, 'index'])->name('contact.index');
Route::get('main',[mainController::class, 'index'])->name('main.index');

