<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->unsignedBigInteger('users_id')->nullable();

            $table->index('users_id', 'leads_users_idx');

            $table->foreign('users_id', 'users_fk')->on('leads')->references('id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leads', function (Blueprint $table) {
            $table->dropForeign('users_fk');
            $table->dropIndex("leads_users_idx");
            $table->dropColumn("users_id");

        });
    }
};
