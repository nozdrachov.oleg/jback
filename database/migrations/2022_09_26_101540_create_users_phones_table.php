<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_phones', function (Blueprint $table) {
            $table->id();
            $table->string('phone');
            $table->string('active');
            $table->timestamps();

            $table->unsignedBigInteger('user_id')->nullable();

            $table->index('user_id', 'users_phone_users_idx');

            $table->foreign('user_id', 'user_fk')->on('users')->references('id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_phones');
    }
};
